 #include "opencv2/core/core.hpp"
 #include "opencv2/contrib/contrib.hpp"
 #include "opencv2/highgui/highgui.hpp"
 #include "opencv2/imgproc/imgproc.hpp"
 #include "opencv2/objdetect/objdetect.hpp"
 
 #include <iostream>
 #include <fstream>
 #include <sstream>
 
 using namespace cv;
 using namespace std;
 
 static void read_csv(const string& filename, vector<Mat>& images, vector<int>& labels, char separator = ';') {
     std::ifstream file(filename.c_str(), ifstream::in);
     if (!file) {
         string error_message = "No valid input file was given, please check the given filename.";
         CV_Error(CV_StsBadArg, error_message);
     }
     string line, path, classlabel;
     while (getline(file, line)) {
         stringstream liness(line);
         getline(liness, path, separator);
         getline(liness, classlabel);
         if(!path.empty() && !classlabel.empty()) {
             images.push_back(imread(path, 0));
             labels.push_back(atoi(classlabel.c_str()));
         }
     }
 }
 
 int main(int argc, const char *argv[]) {
     // Check for valid command line arguments, print usage
     // if no arguments were given.
    cout<<"hello";
     // Get the path to your CSV:

 
     string fn_csv = "C:\\Users\\Ahmad Mustafa\\Desktop\\opencv.csv";
     int deviceId = 0;
 	cout<<"hello";
     // These vectors hold the images and corresponding labels:
     vector<Mat> images;
     vector<int> labels;
     // Read in the data (fails if no valid input filename is given, but you'll get an error message):
     try {
         read_csv(fn_csv, images, labels);
 		cout<<"hello";
     } catch (cv::Exception& e) {
         cerr << "Error opening file \"" << fn_csv << "\". Reason: " << e.msg << endl;
         // nothing more we can do
         exit(1);
     }
     // Get the height from the first image. We'll need this
     // later in code to reshape the images to their original
     // size AND we need to reshape incoming faces to this size:
     int im_width = images[0].cols;
     int im_height = images[0].rows;
 	cout<<"hello";
     // Create a FaceRecognizer and train it on the given images:
     Ptr<FaceRecognizer> model = createLBPHFaceRecognizer();
     model->train(images, labels);
	 model->save("C:\\Users\\Ahmad Mustafa\\Desktop\\model.yml");
	 cout<<"images SIZE+"<<images.size();
     // That's it for learning the Face Recognition model. You now
     // need to create the classifier for the task of Face Detection.
     // We are going to use the haar cascade you have specified in the
     // command line arguments:
     //
 	cout<<"hello";
     CascadeClassifier haar_cascade;
     haar_cascade.load("C:\\opencv\\sources\\data\\haarcascades\\haarcascade_frontalface_alt.xml");
	if(haar_cascade.empty())
	{cout<<"Cant load cascade!";
	return -1;}
	printf("Hello6");
     // Get a handle to the Video device:

    
     // Holds the current frame from the Video device:
     Mat frame=imread("C:\\Users\\Ahmad Mustafa\\Desktop\\4.png",0);
   
         
		if(frame.empty())
		{cout<<"Canot access frame";
		return -1;}
         // Clone the current frame:
         Mat original = frame.clone();
         // Convert the current frame to grayscale:
         Mat gray;

        cvtColor(frame, gray,  CV_RGB2GRAY);
         // Find the faces in the frame:
         vector< Rect_<int> > faces;
         haar_cascade.detectMultiScale(gray, faces);
         // At this point you have the position of the faces in
         // faces. Now we'll get the faces, make a prediction and
         // annotate it in the video. Cool or what?
         for(int i = 0; i < faces.size(); i++) {
             // Process face by face:
             Rect face_i = faces[i];
             // Crop the face from the image. So simple with OpenCV C++:
             Mat face = gray(face_i);
             // Resizing the face is necessary for Eigenfaces and Fisherfaces. You can easily
             // verify this, by reading through the face recognition tutorial coming with OpenCV.
             // Resizing IS NOT NEEDED for Local Binary Patterns Histograms, so preparing the
             // input data really depends on the algorithm used.
             //
             // I strongly encourage you to play around with the algorithms. See which work best
             // in your scenario, LBPH should always be a contender for robust face recognition.
             //
             // Since I am showing the Fisherfaces algorithm here, I also show how to resize the
             // face you have just found:
             //Mat face_resized;
             //cv::resize(face, face_resized, Size(im_width, im_height), 1.0, 1.0, INTER_CUBIC);
             // Now perform the prediction, see how easy that is:
			 int prediction = model->predict(original);
             // And finally write all we've found out to the original image!
             // First of all draw a green rectangle around the detected face:
             rectangle(frame, face_i, CV_RGB(0, 255,0), 1);
             // Create the text we will annotate the box with:
             string box_text = format("Prediction = %d", prediction);
             // Calculate the position for annotated text (make sure we don't
             // put illegal values in there):
             int pos_x = std::max(face_i.tl().x - 10, 0);
             int pos_y = std::max(face_i.tl().y - 10, 0);
             // And now put it into the image:
             putText(frame, box_text, Point(pos_x, pos_y), FONT_HERSHEY_PLAIN, 1.0, CV_RGB(0,255,0), 2.0);
         }
         // Show the result:
         imshow("face_recognizer", frame);
		 waitKey(0);
		
		 cin.ignore();
     
     return 0;
 }